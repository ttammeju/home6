import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Convenience enum for Vertices and Arcs
 * BLANK for un-passed routes
 * TRAVERSED for already passed through routes
 */
enum State {
	BLANK, TRAVERSED
}

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		GraphTask a = new GraphTask();
		a.run();
	}

	/**
	 * Actual main method to run examples and everything.
	 */
	public void run() {
		Graph g = new Graph("G");
		g.createRandomSimpleGraph(5, 7);
		System.out.println(g);

		g.initVertexArcsMap();

		Vertex vRan1 = g.getRandomVertex();
		Vertex vRan2 = g.getRandomVertex();

		List<Stack<Vertex>> allPaths = g.routesFromTo(vRan1, vRan2);
		List<Vertex> bestPath = bestWeightedPathFrom(g, allPaths);

		System.out.println("/ / / / / / / / / / /");
		System.out.println("\tFOUND BEST PATH: ");
		System.out.println(bestPath);
		System.out.println("/ / / / / / / / / / /");
	}

	/**
	 * traverses through all the paths and finds the best one
	 * which has the maximum weight (determined by the lowest weight arc, in a path)
	 * @param   g         Graph
	 * @param   paths     All the possible paths from start to finish
	 * @return  a path which has the maximum weight
	 */
	private List<Vertex> bestWeightedPathFrom(Graph g, List<Stack<Vertex>> paths) {
		int maxWeight = Integer.MIN_VALUE;
		List<Vertex> bestPath = null;

		for (Stack<Vertex> stack : paths) {
			int stacksLowestWeight = Integer.MAX_VALUE;
			for (int j = 0; j < stack.size() - 1; j++) {
				Vertex v1 = stack.get(j);
				Vertex v2 = stack.get(j + 1);
				int weight = g.findVertexWeightBetween(v1, v2);

				if(stacksLowestWeight > weight){
					stacksLowestWeight = weight;
				}
			}
			System.out.println(stack + " smallest weight in path: " + stacksLowestWeight);
			if(maxWeight < stacksLowestWeight){
				maxWeight = stacksLowestWeight;
				bestPath = stack;
			}

		}
		return bestPath;
	}
}

class Graph {
	private Map<Vertex, List<Arc>> vertexArcs = new HashMap<>();
	private int vertices;
	private String id;
	private Vertex first;
	private int info = 0;

	Graph(String s, Vertex v) {
		id = s;
		first = v;
	}

	Graph(String s) {
		this(s, null);
	}

	/**
	 * finds a weight between two directly connected vertices (brothers)
	 * @param   v1  vertex 1
	 * @param   v2  vertex 2
	 * @return  weight between given vertices
	 */
	public int findVertexWeightBetween(Vertex v1, Vertex v2) {
		List<Arc> vs1 = vertexArcs.get(v1);

		for (Arc a : vs1) {
			if (a.id.contains("a" + v1 + "_" + v2)) {
				return a.weight;
			}
		}
		throw new RuntimeException("vertices 'v1' and 'v2' must be direct siblings!");
	}

	/**
	 * finds all the routes between two vertices in a graph
	 * @param   from start of graph traversal
	 * @param   to end of graph traversal
	 * @return  a list containing all the possible paths
	 */
	public List<Stack<Vertex>> routesFromTo(Vertex from, Vertex to) {
		System.out.println("/ / / / / / / / / / /");
		System.out.println("FROM: " + from + " TO: " + to);
		System.out.println("/ / / / / / / / / / /");
		System.out.println();


		List<Stack<Vertex>> paths = new ArrayList<>();
		Stack<Vertex> vs = new Stack<>();
		Vertex v = from;
		vs.add(v);

		while (true) {
			//System.out.println("stack: " + vs);
			List<Arc> arcs = vertexArcs.get(v);
			//System.out.println("traversing: " + v + ", num of arcs: " + arcs.size());
			if (arcs.size() > 0) {
				Arc maxArc = maxWeightFrom(arcs);
				if (maxArc == null) {
					//System.out.println("No arcs left on this node!");
					//System.out.println();
					vs.lastElement().state = State.BLANK;
					if (vs.size() < 2) {
						break;
					}
					vs.pop();
					v = vs.peek();
					continue;
				}
				//System.out.println("connected to: " + maxArc.target);

				if (to.equals(maxArc.target)) {
					//System.out.println();
					//System.out.println("PATH FOUND!");
					paths.add((Stack<Vertex>) vs.clone());
					paths.get(paths.size() - 1).add(to);
					//System.out.println(paths);

					v = vs.peek();
					maxArc.state = State.TRAVERSED;
					//System.out.println();
					continue;
				}

				switch (maxArc.target.state) {
					case BLANK:
						//System.out.println(maxArc);
						//System.out.println();

						v.state = State.TRAVERSED;
						v = maxArc.target;
						vs.add(v);
						break;
					case TRAVERSED:
						//System.out.println("next arc, please!");
						//System.out.println();

						maxArc.state = State.TRAVERSED;
						break;
				}
			}
		}
		return paths;
	}

	/**
	 * finds the arc with maximum weight in a given list of Arcs
	 * @param   vertixArcs  a list containing arcs
	 * @return  arc with maximum weight
	 */
	private Arc maxWeightFrom(List<Arc> vertixArcs) {
		Arc maxArc = new Arc(null);
		maxArc.weight = 0;
		for (Arc a : vertixArcs) {
			if (maxArc.weight < a.weight && a.state != State.TRAVERSED) {
				maxArc = a;
			}
		}
		if (maxArc.weight != 0) {
			maxArc.state = State.TRAVERSED;
			return maxArc;
		} else {
			return null;
		}
	}

	/**
	 * Constructs a Map representation of a Graph
	 * @return graph as a map
	 */
	public Map<Vertex, List<Arc>> initVertexArcsMap() {
		Vertex vertex = this.first;
		for (int i = 0; i < vertices; i++) {
			Vertex v = vertex;

			Arc vv = v.first;
			List<Arc> arcs = new ArrayList<>();

			while (vv != null) {
				arcs.add(vv);
				vv = vv.next;
			}
			vertexArcs.put(v, arcs);

			vertex = v.next;
		}

		return vertexArcs;
	}

	/**
	 * @return random vertex from a graph
	 */
	public Vertex getRandomVertex() {
		int ran = ThreadLocalRandom.current().nextInt(0, vertices);
		return vertexArcs.keySet().toArray(new Vertex[0])[ran];
	}


	@Override
	public String toString() {
		String nl = System.getProperty("line.separator");
		StringBuffer sb = new StringBuffer(nl);
		sb.append(id);
		sb.append(nl);
		Vertex v = first;
		while (v != null) {
			sb.append(v.toString());
			sb.append(" -->");
			Arc a = v.first;
			while (a != null) {
				sb.append(" ");
				sb.append(a.toString());
				sb.append(" (");
				sb.append(v.toString());
				sb.append("->");
				sb.append(a.target.toString());
				sb.append(")");
				a = a.next;
			}
			sb.append(nl);
			v = v.next;
		}
		return sb.toString();
	}

	public Vertex createVertex(String vid) {
		Vertex res = new Vertex(vid);
		res.next = first;
		first = res;
		return res;
	}

	public Arc createArc(String aid, Vertex from, Vertex to) {
		Arc res = new Arc(aid);
		res.next = from.first;
		from.first = res;
		res.target = to;
		res.info = from.info;
		return res;
	}

	/**
	 * Create a connected undirected random tree with n vertices.
	 * Each new vertex is connected to some random existing vertex.
	 *
	 * @param n number of vertices added to this graph
	 */
	public void createRandomTree(int n) {
		if (n <= 0)
			return;
		Vertex[] varray = new Vertex[n];
		for (int i = 0; i < n; i++) {
			varray[i] = createVertex("v" + String.valueOf(n - i));
			if (i > 0) {
				int vnr = (int) (Math.random() * i);
				createArc("a" + varray[vnr].toString() + "_"
						+ varray[i].toString(), varray[vnr], varray[i]);
				createArc("a" + varray[i].toString() + "_"
						+ varray[vnr].toString(), varray[i], varray[vnr]);
			} else {
			}
		}
	}

	/**
	 * Create an adjacency matrix of this graph.
	 * Side effect: corrupts info fields in the graph
	 *
	 * @return adjacency matrix
	 */
	public int[][] createAdjMatrix() {
		info = 0;
		Vertex v = first;
		while (v != null) {
			v.info = info++;
			v = v.next;
		}
		int[][] res = new int[info][info];
		v = first;
		while (v != null) {
			int i = v.info;
			Arc a = v.first;
			while (a != null) {
				int j = a.target.info;
				res[i][j]++;
				a = a.next;
			}
			v = v.next;
		}
		return res;
	}

	/**
	 * Create a connected simple (undirected, no loops, no multiple
	 * arcs) random graph with n vertices and m edges.
	 *
	 * @param n number of vertices
	 * @param m number of edges
	 */
	public void createRandomSimpleGraph(int n, int m) {
		vertices = n;

		if (n <= 0)
			return;
		if (n > 2500)
			throw new IllegalArgumentException("Too many vertices: " + n);
		if (m < n - 1 || m > n * (n - 1) / 2)
			throw new IllegalArgumentException
					("Impossible number of edges: " + m);
		first = null;
		createRandomTree(n);       // n-1 edges created here
		Vertex[] vert = new Vertex[n];
		Vertex v = first;
		int c = 0;
		while (v != null) {
			vert[c++] = v;
			v = v.next;
		}
		int[][] connected = createAdjMatrix();
		int edgeCount = m - n + 1;  // remaining edges
		while (edgeCount > 0) {
			int i = (int) (Math.random() * n);  // random source
			int j = (int) (Math.random() * n);  // random target
			if (i == j)
				continue;  // no loops
			if (connected[i][j] != 0 || connected[j][i] != 0)
				continue;  // no multiple edges
			Vertex vi = vert[i];
			Vertex vj = vert[j];
			createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
			connected[i][j] = 1;
			createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
			connected[j][i] = 1;
			edgeCount--;  // a new edge happily created
		}
	}
}

/**
 * Arc represents one arrow in the graph. Two-directional edges are
 * represented by two Arc objects (for both directions).
 */
class Arc {
	public State state;
	public String id;
	public Vertex target;
	public Arc next;
	public int info = 0;
	public int weight = 0;

	Arc(String s, Vertex v, Arc a) {
		id = s;
		target = v;
		next = a;
		weight = ThreadLocalRandom.current().nextInt(1, 5 + 1);
	}

	Arc(String s) {
		this(s, null, null);
	}

	@Override
	public String toString() {
		return id + " weight: " + weight;
	}
}

// TODO!!! add javadoc relevant to your problem
class Vertex {
	public State state = State.BLANK;
	public String id;
	public Vertex next;
	public Arc first;
	public int info = 0;

	Vertex(String s, Vertex v, Arc e) {
		id = s;
		next = v;
		first = e;
		info = 0;

	}

	Vertex(String s) {
		this(s, null, null);
	}

	@Override
	public String toString() {
		return id;
	}
}